import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  username:string;
  password:string;

  constructor(public navCtrl: NavController, public http: HttpClient) { }

  ngOnInit() {
  }

  login(){
    console.log("Username: ", this.username);
    console.log("Password: ", this.password);
    // console.log("userRole: ", localStorage.getItem('userRole'));
    if(!this.username || !this.password){
      return;
    }
    var url = "http://localhost:5100/api/login";
    var body = {
      "username" : this.username, 
      "password" : this.password,
      //"userRole" : localStorage.getItem('userRole')
    };
    console.log(body);
    var data = this.http.post(url, body);
    data.subscribe(data => {
      console.log(data);
      if (data['message']) {
        alert("Username / password invalid.");
      }
      else if(data['user_id']) {
        localStorage.setItem('username', this.username);
        localStorage.setItem('name', data['name']);
        localStorage.setItem('user_id', data['user_id']);

        if(data['userRole'] == 'student'){
          localStorage.setItem('matricNo', data['matricNo']);
          this.navCtrl.navigateRoot('/dash-stud');

        }

        if(data['userRole'] == 'lecturer'){
          this.navCtrl.navigateRoot('/dash-lect');
        }

        if(data['userRole'] == 'staff'){
          this.navCtrl.navigateRoot('/dash-staf');
        }
      }
    });
  }

  forgotPwPage(){
    this.navCtrl.navigateRoot('/forgot-pw');
  }

}
