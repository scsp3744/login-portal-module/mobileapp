import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-dash-lect',
  templateUrl: './dash-lect.page.html',
  styleUrls: ['./dash-lect.page.scss'],
})
export class DashLectPage implements OnInit {

  name: string;
  
  constructor(public navCtrl: NavController, public http: HttpClient) { }

  ngOnInit() {
    this.name = localStorage.getItem('name');
  }

  // ionViewWillEnter() {
  //   this.lects = [];
  //   if (!localStorage.getItem('token')) {
  //     this.navCtrl.navigateRoot('/home');
  //   }
  //   var url = "http://localhost:5100/api/dash";
  //   this.http.get(url).subscribe(data => {
  //     if (data['message']) {
  //       this.message = data['message'];
  //     }
  //     else {
  //       this.lects = data['results'];
  //       console.log(this.lects);
  //     }
  //   })
  // }

  profile(){
    this.navCtrl.navigateRoot('/profile-lect');
  }

}
