import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashLectPage } from './dash-lect.page';

const routes: Routes = [
  {
    path: '',
    component: DashLectPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashLectPageRoutingModule {}
