import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-dash-staf',
  templateUrl: './dash-staf.page.html',
  styleUrls: ['./dash-staf.page.scss'],
})
export class DashStafPage implements OnInit {

  name: string;
  
  constructor(public navCtrl: NavController, public http: HttpClient) { }

  ngOnInit() {
    this.name = localStorage.getItem('name');
  }

  profile(){
    this.navCtrl.navigateRoot('/profile-staf');
  }

}
