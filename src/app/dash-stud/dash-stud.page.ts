import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-dash-stud',
  templateUrl: './dash-stud.page.html',
  styleUrls: ['./dash-stud.page.scss'],
})
export class DashStudPage implements OnInit {

  name: string;
  matricNo: string;
  
  constructor(public navCtrl: NavController, public http: HttpClient) { }

  ngOnInit() {
    this.name = localStorage.getItem('name');
    this.matricNo = localStorage.getItem('matricNo');
  }

  profile(){
    this.navCtrl.navigateRoot('/profile-stud');
  }

}
