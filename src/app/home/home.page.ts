import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  // constructor(public navCtrl: NavController, public http: HttpClient) { }

  // student(){
  //   localStorage.setItem('userRole', 'student');
  //   this.navCtrl.navigateRoot('/login');
  // }

  // lecturer(){
  //   localStorage.setItem('userRole', 'lecturer');
  //   this.navCtrl.navigateRoot('/login');
  // }

  // staff(){
  //   localStorage.setItem('userRole', 'staff');
  //   this.navCtrl.navigateRoot('/login');
  // }

  username: string;
  password: string;

  constructor(public navCtrl: NavController, public http: HttpClient) { }

  ngOnInit() {
  }

  login() {
    console.log("Username: ", this.username);
    console.log("Password: ", this.password);
    // console.log("userRole: ", localStorage.getItem('userRole'));
    if (!this.username || !this.password) {
      alert("Both username and password are required.");
      return;
    }
    var url = "http://localhost:5100/api/login";
    var body = {
      "username": this.username,
      "password": this.password,
      //"userRole" : localStorage.getItem('userRole')
    };
    console.log(body);
    var data = this.http.post(url, body);
    data.subscribe(data => {
      if (data['user_id']) {
        localStorage.setItem('username', this.username);
        localStorage.setItem('name', data['name']);
        localStorage.setItem('user_id', data['user_id']);
        localStorage.setItem('userRole', data['userRole']);

        if (data['userRole'] == 'student') {
          localStorage.setItem('matricNo', data['matricNo']);
          this.navCtrl.navigateRoot('/dash-stud');

        }

        if (data['userRole'] == 'lecturer') {
          this.navCtrl.navigateRoot('/dash-lect');
        }

        if (data['userRole'] == 'staff') {
          this.navCtrl.navigateRoot('/dash-staf');
        }
      }
    }, error => {
      alert(error["error"]['message']);
    });
  }

  forgotPwPage() {
    this.navCtrl.navigateRoot('/forgot-pw');
  }

}