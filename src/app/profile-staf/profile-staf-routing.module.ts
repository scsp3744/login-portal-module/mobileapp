import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileStafPage } from './profile-staf.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileStafPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileStafPageRoutingModule {}
