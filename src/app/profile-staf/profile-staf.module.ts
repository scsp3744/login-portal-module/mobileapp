import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileStafPageRoutingModule } from './profile-staf-routing.module';

import { ProfileStafPage } from './profile-staf.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileStafPageRoutingModule
  ],
  declarations: [ProfileStafPage]
})
export class ProfileStafPageModule {}
