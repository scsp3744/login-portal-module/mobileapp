import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-profile-staf',
  templateUrl: './profile-staf.page.html',
  styleUrls: ['./profile-staf.page.scss'],
})
export class ProfileStafPage implements OnInit {
  name: string;

  constructor(public navCtrl: NavController, public http: HttpClient) { }

  ngOnInit() {
    this.name = localStorage.getItem('name');
  }

  logout() {
    this.navCtrl.navigateRoot('/home');
  }

  changePwPage(){
    this.navCtrl.navigateRoot('/change-pw');
  }

}
