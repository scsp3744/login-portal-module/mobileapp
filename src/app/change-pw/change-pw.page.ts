import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-change-pw',
  templateUrl: './change-pw.page.html',
  styleUrls: ['./change-pw.page.scss'],
})
export class ChangePwPage implements OnInit {

  username: string;
  password: string;
  newpassword: string;
  newpassword2: string;

  constructor(public navCtrl: NavController, public http: HttpClient) { }
  
  ngOnInit() {
    this.username = localStorage.getItem('username');
  }

  changePw(){
    console.log("Password: ", this.password);
    console.log("NewPassword: ", this.newpassword);
    console.log("NewPassword2: ", this.newpassword2);
    if(this.newpassword != this.newpassword2){
      alert("Please make sure your passwords match");
    }
    else{
      var url = "http://localhost:5100/api/changePw";
      var body = {
        "username" : this.username,
        "password" : this.password,
        "newpassword" : this.newpassword
      };
      console.log(body);
      var data = this.http.post(url, body);
      data.subscribe(data => {
        console.log(data);
        if (data['message'] == "no") {
          alert("Cannot change password");
        }
        else if(data['message'] == "yes") {
          alert("Password changed successfully");
          if(localStorage.getItem('userRole') == 'student'){
            this.navCtrl.navigateRoot('/profile-stud');
          }
      
          if(localStorage.getItem('userRole') == 'lecturer'){
            this.navCtrl.navigateRoot('/profile-lect');
          }
      
          if(localStorage.getItem('userRole') == 'staff'){
            this.navCtrl.navigateRoot('/profile-staf');
          }
        }
      });
    }
  }

  goBack(){
    if(localStorage.getItem('userRole') == 'student'){
      this.navCtrl.navigateRoot('/profile-stud');
    }

    if(localStorage.getItem('userRole') == 'lecturer'){
      this.navCtrl.navigateRoot('/profile-lect');
    }

    if(localStorage.getItem('userRole') == 'staff'){
      this.navCtrl.navigateRoot('/profile-staf');
    }
  }
}
