import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'dash-lect',
    loadChildren: () => import('./dash-lect/dash-lect.module').then( m => m.DashLectPageModule)
  },
  {
    path: 'change-pw',
    loadChildren: () => import('./change-pw/change-pw.module').then( m => m.ChangePwPageModule)
  },
  {
    path: 'profile-lect',
    loadChildren: () => import('./profile-lect/profile-lect.module').then( m => m.ProfileLectPageModule)
  },
  {
    path: 'forgot-pw',
    loadChildren: () => import('./forgot-pw/forgot-pw.module').then( m => m.ForgotPwPageModule)
  },
  {
    path: 'dash-stud',
    loadChildren: () => import('./dash-stud/dash-stud.module').then( m => m.DashStudPageModule)
  },
  {
    path: 'dash-staf',
    loadChildren: () => import('./dash-staf/dash-staf.module').then( m => m.DashStafPageModule)
  },
  {
    path: 'profile-stud',
    loadChildren: () => import('./profile-stud/profile-stud.module').then( m => m.ProfileStudPageModule)
  },
  {
    path: 'profile-staf',
    loadChildren: () => import('./profile-staf/profile-staf.module').then( m => m.ProfileStafPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
