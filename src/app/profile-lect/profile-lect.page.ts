import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-profile-lect',
  templateUrl: './profile-lect.page.html',
  styleUrls: ['./profile-lect.page.scss'],
})
export class ProfileLectPage implements OnInit {
  name: string;

  constructor(public navCtrl: NavController, public http: HttpClient) { }

  ngOnInit() {
    this.name = localStorage.getItem('name');
  }

  logout() {
    this.navCtrl.navigateRoot('/home');
  }

  changePwPage(){
    this.navCtrl.navigateRoot('/change-pw');
  }

}
