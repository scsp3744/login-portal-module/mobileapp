import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-profile-stud',
  templateUrl: './profile-stud.page.html',
  styleUrls: ['./profile-stud.page.scss'],
})
export class ProfileStudPage implements OnInit {
  name: string;
  matricNo: string;

  constructor(public navCtrl: NavController, public http: HttpClient) { }

  ngOnInit() {
    this.name = localStorage.getItem('name');
    this.matricNo = localStorage.getItem('matricNo');
  }

  logout() {
    this.navCtrl.navigateRoot('/home');
  }

  changePwPage(){
    this.navCtrl.navigateRoot('/change-pw');
  }

}
